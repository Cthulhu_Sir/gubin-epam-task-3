package ru.spb.epam.gubin.task3.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class AlbumsList {

    private static List<Album> albums = new LinkedList<>();

    public static List getAlbumsList(){
        albums.clear();

        DBConnection.connect();
        String query = "SELECT * FROM albums";

        ResultSet resSet = DBConnection.getByQuery(query);

        try {
            while(resSet.next()) {

                Album album = new Album(resSet.getInt("id"), resSet.getString("name"), resSet.getString("author"));
                albums.add(album);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        DBConnection.disconnect();

        return albums;
    }
}
