package ru.spb.epam.gubin.task3.classes;


import java.sql.ResultSet;
import java.sql.SQLException;

public class NewFilePath {

    public static String newFileEditPage() {

        int newID = lastAddedId() + 1;

        return "edit?id=" + newID;
    }

    public static int lastAddedId() {
        DBConnection.connect();
        ResultSet resSet = DBConnection.getByQuery("SELECT id FROM songs ORDER BY id DESC");

        int lastAdded = 0;

        try {
            while (resSet.next()) {
                lastAdded = resSet.getInt("id");
                if (resSet.isFirst()) {
                    break;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        DBConnection.disconnect();

        return lastAdded;
    }

    public static int nextVacantId() {
        return lastAddedId() + 1;
    }
}
