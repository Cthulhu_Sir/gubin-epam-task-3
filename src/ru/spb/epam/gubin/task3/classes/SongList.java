package ru.spb.epam.gubin.task3.classes;

import java.util.*;

public class SongList {
    private static Map<Integer, Song> songs = new HashMap<>();

    public static Map getSongs(){
        return songs;
    }

    public static void clearSongList(){
        songs.clear();
    }

    public static void addSong(Song song){
        songs.put(song.getId(), song);
    }

}
