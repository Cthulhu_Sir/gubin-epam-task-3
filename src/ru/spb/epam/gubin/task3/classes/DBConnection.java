package ru.spb.epam.gubin.task3.classes;

import java.io.InputStream;
import java.sql.*;
import java.util.Scanner;

public class DBConnection {
    private static final String url = "jdbc:mysql://localhost/task3db?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Moscow";
    private static final String user = "Cthulhu_Sir";
    private static final String password = "jbnmocew";

    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;

    public static void connect(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);

            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet getByQuery(String query){
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);

        } catch (SQLException ex){
            ex.printStackTrace();
        }
        return rs;
    }

    public static void setByQuery(String query){
        try {
            stmt = con.createStatement();
            stmt.executeUpdate(query);

        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public static void disconnect(){
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String inputStreamToString(InputStream inputStream){
        Scanner scanner = new Scanner(inputStream);
        scanner.useDelimiter("\\Z");//To read all scanner content in one String
        String data = "";
        if (scanner.hasNext())
            data = scanner.next();
        return data;
    }

    public static void delete(String request){

    }
}
