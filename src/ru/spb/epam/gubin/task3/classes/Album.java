package ru.spb.epam.gubin.task3.classes;

public class Album {

    private int id;
    private String name;
    private String band;

    public Album(int id, String name, String band) {
        this.id = id;
        this.name = name;
        this.band = band;
    }

    public Album() {
    }

    public String getBand() {
        return band;
    }

    public void setBand(String band) {
        this.band = band;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
