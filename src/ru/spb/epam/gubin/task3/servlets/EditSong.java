package ru.spb.epam.gubin.task3.servlets;

import ru.spb.epam.gubin.task3.classes.Album;
import ru.spb.epam.gubin.task3.classes.AlbumsList;
import ru.spb.epam.gubin.task3.classes.DBConnection;
import ru.spb.epam.gubin.task3.classes.Song;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/EditSong")
public class EditSong extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int id = 0;
        try {
            id = (int)request.getAttribute("id");
        } catch (Exception e) {
            id = Integer.parseInt(request.getParameter("id"));
        }

        String query = "SELECT * FROM songs WHERE id="+id;

        DBConnection.connect();
        ResultSet resSet = DBConnection.getByQuery(query);

        Song song = new Song();

        try {
            resSet.next();

            song.setId(resSet.getInt("id"));
            song.setSongName(DBConnection.inputStreamToString(resSet.getAsciiStream("name")));
            song.setAlbum(DBConnection.inputStreamToString(resSet.getAsciiStream("album")));
            song.setAuthor(DBConnection.inputStreamToString(resSet.getAsciiStream("author")));

        } catch (SQLException e) {
            e.printStackTrace();
        }


        request.setAttribute("song", song);

        List<Album> albums = AlbumsList.getAlbumsList();

        request.setAttribute("albums", albums);

        DBConnection.disconnect();
        request.getRequestDispatcher("EditSong.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
