package ru.spb.epam.gubin.task3.servlets;

import ru.spb.epam.gubin.task3.classes.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/AddAlbum")
public class AddAlbum extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String title = (String)request.getParameter("title");
        String band = (String)request.getParameter("band");

        String query = "INSERT INTO albums (name, author) VALUES ('"+title+"', '"+band+"')";

        DBConnection.connect();
        DBConnection.setByQuery(query);
        DBConnection.disconnect();

        response.sendRedirect("index");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
