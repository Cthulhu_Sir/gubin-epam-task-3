package ru.spb.epam.gubin.task3.servlets;

import java.sql.*;

import ru.spb.epam.gubin.task3.classes.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@WebServlet("/index")
public class ShowSongs extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        List<Album> albums = AlbumsList.getAlbumsList();


        String[] albumsArray = request.getParameterValues("albums[]");
        List<String> albumsToShow = null;
        if (!Objects.isNull(albumsArray)) {
            albumsToShow = new LinkedList<>(Arrays.asList(albumsArray));
        }

        String show = request.getParameter("show");

        if (Objects.isNull(show) || show.equals("songs")) {
            DBConnection.connect();
            String query = "select * from songs";
            ResultSet resSet = DBConnection.getByQuery(query);

            SongList.clearSongList();

            try {
                while (resSet.next()) {

                    Song song = new Song();
                    String songPath = resSet.getString("path");
                    int songId = resSet.getInt("id");

                    song.setPath(new File(songPath));
                    song.setId(songId);
                    song.setSongName(DBConnection.inputStreamToString(resSet.getAsciiStream("name")));
                    song.setAlbum(DBConnection.inputStreamToString(resSet.getAsciiStream("album")));
                    song.setAuthor(DBConnection.inputStreamToString(resSet.getAsciiStream("author")));


                    if (Objects.isNull(albumsToShow) || albumsToShow.contains("" + resSet.getInt("albumid")) || albumsToShow.contains("")) {
                        SongList.addSong(song);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            request.setAttribute("songs", SongList.getSongs());
            request.setAttribute("show", "songs");

        } else {
            request.setAttribute("show", "albums");
        }


        DBConnection.disconnect();

        request.setAttribute("albums", albums);
        request.getRequestDispatcher("ShowSongs.jsp").forward(request, response);
    }
}
