package ru.spb.epam.gubin.task3.servlets;

import ru.spb.epam.gubin.task3.classes.Album;
import ru.spb.epam.gubin.task3.classes.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/EditAlbum")
public class EditAlbum extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("id"));

        Album album = null;
        if (id!=1) {
            String query = "SELECT * FROM albums WHERE id="+id;

            DBConnection.connect();
            ResultSet resSet = DBConnection.getByQuery(query);

            album = null;
            try {
                resSet.next();
                album = new Album(resSet.getInt("id"), resSet.getString("name"), resSet.getString("author"));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            request.setAttribute("album", album);
            getServletContext().getRequestDispatcher("/EditAlbum.jsp").forward(request,response);
        }

        response.sendRedirect("/index");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
