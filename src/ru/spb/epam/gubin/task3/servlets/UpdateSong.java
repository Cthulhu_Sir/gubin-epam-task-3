package ru.spb.epam.gubin.task3.servlets;

import ru.spb.epam.gubin.task3.classes.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/UpdateSong")
public class UpdateSong extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");
        String band = request.getParameter("band");
        String albumId = request.getParameter("albumId");
        String albumName = "unsorted";


        String query = "SELECT * FROM albums WHERE id="+albumId;

        DBConnection.connect();
        ResultSet reSet = DBConnection.getByQuery(query);

        try {
            reSet.next();
            albumName = reSet.getString("name");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        query = "UPDATE songs SET name='"+title+"', album='"+albumName+"', albumid='"+albumId+"', author='"+band+"' WHERE id="+id;

        DBConnection.setByQuery(query);
        DBConnection.disconnect();

        response.sendRedirect("/index.jsp");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
