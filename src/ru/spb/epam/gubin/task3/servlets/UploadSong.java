package ru.spb.epam.gubin.task3.servlets;

import ru.spb.epam.gubin.task3.classes.DBConnection;
import ru.spb.epam.gubin.task3.classes.NewFilePath;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;

@WebServlet("/UploadSong")
@MultipartConfig
public class UploadSong extends HttpServlet {

    private static final String SAVE_DIR = "resources";

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length()-1);
            }
        }
        return "";
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// gets absolute path of the web application
        String appPath = request.getServletContext().getRealPath("");
        // constructs path of the directory to save uploaded file
        String savePath = appPath + File.separator + SAVE_DIR;

        // creates the save directory if it does not exists
        File fileSaveDir = new File("/resources");
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
        }

        String fileName ="";
        for (Part part : request.getParts()) {
            fileName = extractFileName(part);
            // refines the fileName in case it is an absolute path
            fileName = new File(fileName).getName();
            part.write(savePath + File.separator + fileName);
        }

        String query = "INSERT INTO songs (name, path) VALUES ('"+ fileName +"','/resources/"+ fileName +"')";

        DBConnection.connect();
        DBConnection.setByQuery(query);
        DBConnection.disconnect();

        request.setAttribute("id", NewFilePath.lastAddedId());
        getServletContext().getRequestDispatcher("/EditSong").forward(
                request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
