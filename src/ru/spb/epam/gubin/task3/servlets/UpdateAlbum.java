package ru.spb.epam.gubin.task3.servlets;

import ru.spb.epam.gubin.task3.classes.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/UpdateAlbum")
public class UpdateAlbum extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("title");
        String author = request.getParameter("band");

        String query = "UPDATE albums SET name='" + name + "', author='" + author + "' WHERE id=" + id;

        DBConnection.connect();
        DBConnection.setByQuery(query);

        query = "UPDATE songs SET album='" + name + "' WHERE albumid=" + id;
        DBConnection.setByQuery(query);

        DBConnection.disconnect();

        response.sendRedirect("index?show=albums");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
