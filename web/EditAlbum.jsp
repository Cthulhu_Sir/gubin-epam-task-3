<%--
  Created by IntelliJ IDEA.
  User: Cthulhu_Sir
  Date: 03.06.2018
  Time: 19:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Album</title>
</head>
<body>
<form action="UpdateAlbum" method="post">
    <p>
        <input type="hidden" name="id" value="${requestScope.album.id}">
        <input name="title" value="${requestScope.album.name}">
        <input name="band" value="${requestScope.album.band}">
    </p>
    <p><input type="submit"></p>
</form>
</body>
</html>
