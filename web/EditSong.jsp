<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Cthulhu_Sir
  Date: 02.06.2018
  Time: 2:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>EDIT</title>
</head>
<body>
    <form action="UpdateSong" method="post">

        <input type="hidden" name="id" value="${requestScope.song.id}">

        <table>
            <td>
                <tr>Song Title: </tr><tr><input name="title" type="text" value="${requestScope.song.songName}"></tr>
            </td>
            <br><br>
            <td>
                <tr>Band/Author: </tr><tr><input name="band" type="text" value="${requestScope.song.author}"></tr>
            </td>
            <br><br>
            <td>
                <tr>Album: </tr>
                <tr>
                    <select size="1" required name="albumId">
                        <c:forEach items="${requestScope.albums}" var="album">
                            <option value="${album.id}">${album.name}</option>
                        </c:forEach>
                    </select>
                </tr>
            </td>
            <br><br>
            <td>
                <tr><input type="submit"></tr>
            </td>
        </table>
    </form>
</body>
</html>
