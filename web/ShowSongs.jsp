<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Cthulhu_Sir
  Date: 01.06.2018
  Time: 16:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SONGS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<table class="table table-hover">
    <c:choose>
        <c:when test="${requestScope.show eq 'songs'}">
            <thead>
            <tr>
                <td>
                    <form action="/index" method="get">
                        <input name="show" value="albums" type="hidden">
                        <input type="submit" value="Album List">
                    </form>
                </td>
                <form action="index" method="get">
                    <td>
                        <select size="4" multiple class="form-control" name="albums[]">
                            <option value="" selected>----ALL----</option>
                            <c:forEach items="${requestScope.albums}" var="album">
                                <option value="${album.id}">${album.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td>
                        <input type="submit" value="filter">
                    </td>
                </form>
            </tr>

            <tr>

                <th>Band/Author</th>
                <th>Song</th>
                <th>Album</th>
                <th>Play</th>
                <th>
                    <form action="Upload.jsp">
                        <input type="submit" value="Add Song">
                    </form>
                </th>
                <th>
                    <form action="AddAlbum.jsp">
                        <input type="submit" value="Add Album">
                    </form>
                </th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.songs}" var="song">
                <tr>
                    <td>
                            ${song.value.author}
                    </td>
                    <td>
                            ${song.value.songName}
                    </td>
                    <td>
                            ${song.value.album}
                    </td>
                    <td>
                        <audio src="${song.value.path}" controls></audio>
                    </td>
                    <td>
                        <form action="EditSong" method="post">
                            <input name="id" type="hidden" value="${song.value.id}">
                            <input type="submit" value="Edit">
                        </form>
                    </td>
                    <td>
                        <form action="DeleteSong" method="post">
                            <input name="id" type="hidden" value="${song.value.id}">
                            <input type="submit" value="Delete">
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </c:when>
        <c:otherwise>
            <thead>
            <tr>
                <form action="index" method="get">
                    <td>
                        <input type="hidden" name="show" value="songs">
                        <input type="submit" value="Song List">
                    </td>
                </form>
            </tr>

            <tr>

                <th>Band/Author</th>
                <th>Album</th>
                <th>
                    <form action="Upload.jsp">
                        <input type="submit" value="Add Song">
                    </form>
                </th>
                <th>
                    <form action="AddAlbum.jsp">
                        <input type="submit" value="Add Album">
                    </form>
                </th>
            </tr>
            </thead>
            <tbody>

                <c:forEach items="${requestScope.albums}" var="album">
                <tr>

                    <td>
                            ${album.band}
                    </td>
                    <td>
                            ${album.name}
                    </td>

                    <td>
                        <form action="EditAlbum" method="post">
                            <input type="hidden" name="id" value="${album.id}">
                            <input type="submit" value="edit">
                        </form>
                    </td>
                    <td>
                        <form action="DeleteAlbum" method="post">
                            <input type="hidden" name="id" value="${album.id}">
                            <input type="submit" value="delete">
                        </form>
                    </td>

                </tr>
                </c:forEach>
            </tbody>
        </c:otherwise>
    </c:choose>
</table>
</body>
</html>
